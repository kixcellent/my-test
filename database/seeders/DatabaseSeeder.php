<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        DB::table('users')->insert([
            "name" => 'Ruahden',
            "username" => 'ruah',
            "password" => '$2y$10$JbUGPTOCcfrOqPtTkvSVy.2WQu/C4snJPSnSVrt2wQpyW1S91CQU6',
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);

        DB::table('reasons')->insert([
            [
                'symbol' => 'A',
                'name' => 'Adult',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'symbol' => 'B',
                'name' => 'Child',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'symbol' => 'C',
                'name' => 'Neurology',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'symbol' => 'D',
                'name' => 'Geriatrics',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'symbol' => 'E',
                'name' => 'Refill of Medications',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'symbol' => 'F',
                'name' => 'Document Request',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'symbol' => 'G',
                'name' => 'Admission',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'symbol' => 'H',
                'name' => 'Others',
                'active' => true,
                'created_at' => Carbon::now(),
            ],
        ]);

        DB::table('windows')->insert([
            [
                'number' => 1,
                'name' => 'Window 1',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'number' => 2,
                'name' => 'Window 2',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'number' => 3,
                'name' => 'Window 3',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'number' => 4,
                'name' => 'Window 4',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'number' => 5,
                'name' => 'Window 5',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'number' => 6,
                'name' => 'Window 6',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'number' => 7,
                'name' => 'Window 7',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'number' => 8,
                'name' => 'Window 8',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'number' => 9,
                'name' => 'Window 9',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'number' => 10,
                'name' => 'Window 10',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'number' => 11,
                'name' => 'Window 11',
                'active' => true,
                'created_at' => Carbon::now(),
            ], [
                'number' => 12,
                'name' => 'Window 12',
                'active' => true,
                'created_at' => Carbon::now(),
            ],
        ]);
    }
}
