<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'reason',
        'number',
        'type',
        'hpercode',
        'name',
        'contact',
        'pregnant',
        'window',
        'status',
    ];

    /**
     * Get the queue that has a window.
     */
    public function window()
    {
        return $this->belongsTo(Window::class, 'window', 'id');
    }

    /**
     * Get the queue that has a reason.
     */
    public function reason()
    {
        return $this->belongsTo(Window::class, 'reason', 'symbol');
    }
}
