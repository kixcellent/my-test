<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Queue;
use App\Models\Window;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Illuminate\Support\Facades\DB;

class QueueController extends Controller
{
    public function index(Request $request)
    {
        $windows = Window::where('active', true)->get();
        $temp = array();
        foreach ($windows as $window) {
            $temp[$window['number']] = $window['name'];
        }

        $windows = $temp;

        $search = ($request->search ? $request->search : '');
        return Inertia::render('Queue/Index', [
            'patients' => QueryBuilder::for(Queue::class)
                ->allowedFilters(AllowedFilter::exact('window', 'windows.number'))
                ->select('queues.*', DB::raw('queues.id as patientId, reasons.name as reasonName, windows.number as windowNumber, CONCAT(queues.reason, queues.number) as queueNo'))
                ->leftJoin('reasons', 'queues.reason', '=', 'reasons.symbol')
                ->leftJoin('windows', 'queues.window', '=', 'windows.number')
                ->whereDate('queues.created_at', Carbon::today())
                ->whereRaw("CONCAT(queues.reason, queues.number) LIKE '%" . $search . "%'")
                ->allowedSorts([
                    'queueNo',
                    AllowedSort::field('hospitalNo', 'queues.hpercode'),
                    AllowedSort::field('time', 'queues.updated_at'),
                    AllowedSort::field('patientName', 'queues.name'),
                    AllowedSort::field('reasonForVisit', 'reasons.name'),
                    AllowedSort::field('location', 'windows.number'),
                ])
                ->defaultSort('queueNo')
                ->paginate($request->limit),
            'windows' => $windows,
        ]);
    }

    public function filtering(Request $request)
    {
        $keyword = $request->search;
        $paging = $request->page;
        $limit = $request->limit;
        $sort = $request->sort['order'] . $request->sort['data'];
        $filterings = $request->filter;
        $urlQuery = "?";

        //filtering
        foreach ($filterings as $filter) {

            $filterType = $filter['type'];
            $filterData = $filter['fdata'];

            $urlQuery .= "filter[" . $filterType . "]=" . $filterData;

            $urlQuery .= "&";
        }

        //searching
        $urlQuery .= "search=" . $keyword;

        //pagination
        $urlQuery .= "&page=" . $paging;

        //page limit
        $urlQuery .= "&limit=" . $limit;

        //sorting
        $urlQuery .= "&sort=" . $sort;

        return redirect('/queue' . $urlQuery);
    }

    public function transfer(Request $request)
    {
        Queue::where('id', $request->patientId)
            ->update(['window' => $request->window, 'status' => 'waiting']);

        return back();
    }

    public function existStatus(Request $request)
    {
        return Queue::whereDate('created_at', Carbon::today())
            ->where('window', $request->window)
            ->where('status', $request->status)
            ->exists();
    }

    public function updateStatus(Request $request)
    {
        Queue::where('id', $request->patientId)
            ->update(['status' => $request->status]);

        return back();
    }
}
