<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Reason;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Models\Queue;
use Carbon\Carbon;

class TriageController extends Controller
{
    private $title;

    public function __construct()
    {
        $this->title = 'Triage';
    }

    public function index()
    {
        return Inertia::render('Triage/Index', [
            'title' => $this->title,
            'header' => 'Triage',
            'options' => Reason::all(['id', 'symbol', 'name']),
        ]);
    }

    public function store(Request $request)
    {
        $lastQueueNumber = Queue::where('reason', $request->reason)
            ->whereDate('created_at', Carbon::today())
            ->max('number');
        $lastQueueNumber++;

        $queue = Queue::create([
            'reason' => $request->reason,
            'number' => $lastQueueNumber,
            'type' => $request->patientType,
            'hpercode' => $request->hpercode,
            'name' => $request->name,
            'contact' => $request->contact,
            'pregnant' => $request->pregnant,
            'window' => 1,
            'status' => 'waiting',
        ]);

        return Inertia::render(
            'Triage/PrintQueue',
            [
                'title' => $this->title,
                'header' => 'Print Queue Number',
                'symbol' => $queue->reason,
                'reason' => Reason::where('symbol', $queue->reason)->limit(1)->get('name')[0]['name'],
                'number' => $queue->number,
                'patientType' => $queue->type,
                'hpercode' => $queue->hpercode,
                'name' => $queue->name,
                'contact' => $queue->contact,
                'pregnant' => $queue->pregnant,
            ]
        );
    }
}
