<?php

namespace App\Http\Controllers;

use App\Models\Queue;
use Inertia\Inertia;
use Spatie\QueryBuilder\QueryBuilder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        return Inertia::render('Dashboard/Index', [
            'title' => 'Dashboard',
            'canLogin' => true,
        ]);
    }

    public function getDashboardData()
    {
        $patients =  QueryBuilder::for(Queue::class)
        ->select('queues.*', DB::raw('windows.name as windowName'))
            ->leftJoin('windows', 'queues.window', '=', 'windows.number')
            ->whereDate('queues.created_at', Carbon::today())
            ->where('status', 'calling')
            ->orderBy('queues.updated_at', 'desc')
            ->limit(5)
            ->get();
        return $patients;
    }
}
