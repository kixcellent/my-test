<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\QueueController;
use App\Http\Controllers\TriageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// });

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('/dash-data', [DashboardController::class, 'getDashboardData'])->name('dash.data');
Route::redirect('/', '/dashboard');

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/triage', [TriageController::class, 'index'])->name('triage');
    Route::post('/triage', [TriageController::class, 'store']);

    // Links for Queue
    Route::get('/queue', [QueueController::class, 'index'])->name('queue');
    Route::post('/transfer', [QueueController::class, 'transfer'])->name('transfer');
    Route::post('/update-status', [QueueController::class, 'updateStatus'])->name('update.status');
    Route::post('/exist-status', [QueueController::class, 'existStatus'])->name('exist.status');
    Route::post('/filter', [QueueController::class, 'filtering'])->name('filter');
    // End for Queue
});

require __DIR__ . '/auth.php';
